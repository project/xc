<?php
/**
 * @file
 * Selection bar template
 *
 * @copyright (c) 2010-2011 eXtensible Catalog Organization
 */
?>
<div class="selection-bar">
  <ul class="selection-bar-actions">
    <li class="bookmark-action">Save</li>
    <li class="select-all-action">Select all</li>
    <li class="select-none-action">Clear all</li>
<!--
    <li id="email-action">email</li>
    <li id="print-action">print</li>
-->
  </ul>
</div>
