<?php
/**
 * @file
 * XC SQL module functions
 *
 * @copyright (c) 2010-2011 eXtensible Catalog Organization
 */

function xc_sql_init() {
  global $_xc_sql_batch_sql, $_xc_sql_batch_sql_array;

  $xc_sql_batch_sql = array();
  $xc_sql_batch_sql_array = array();
}

function xc_sql_get_location_id($storage_id) {
  static $cache;
  if (empty($cache[$storage_id])) {
    $sql = 'SELECT location_id FROM {xc_sql_storage} WHERE storage_id = %d';
    $cache[$storage_id] = db_result(db_query($sql, $storage_id));
  }
  return $cache[$storage_id];
}

function xc_sql_get_storage_id($location_id) {
  static $cache;
  if (empty($cache[$location_id])) {
    $sql = 'SELECT storage_id FROM {xc_sql_storage} WHERE location_id = %d';
    $cache[$location_id] = db_result(db_query($sql, $location_id));
  }
  return $cache[$location_id];
}

function xc_sql_storage_set($storage) {
  $sql = 'SELECT storage_id FROM {xc_sql_storage} WHERE storage_id = %d';

  $update = empty($storage->storage_id) ? FALSE : db_result(db_query($sql, $storage->storage_id));

  if ($update) {
    $result = TRUE;
  }
  elseif ($storage->location_id) {
    $record = new stdClass();
    $record->location_id = $storage->location_id;
    $result = drupal_write_record('xc_sql_storage', $record);
  }

  if (!$result) {
    drupal_set_message(t('Unexpected error. Unable to create SQL storage'), 'error');
  }
}

function xc_sql_storage_unset($storage) {
  $sql = 'DELETE FROM {xc_sql_storage} WHERE storage_id = %d';
  $result = db_query($sql, $storage->storage_id);
  if ($result) {
    drupal_set_message(t('SQL storage deleted'));
  }
  else {
    drupal_set_message(t('Unexpected error. Unable to delete SQL storage'), 'error');
  }
}

function xc_sql_storage_empty($storage) {
  $sql = 'DELETE FROM {xc_sql_metadata} WHERE storage_id = %d';
  $result = db_query($sql, $storage->storage_id);
  if ($result) {
    drupal_set_message(t('All metadata removed from SQL storage'));
  }
  else {
    drupal_set_message(t('Unexpected error. Unable to empty SQL storage'), 'error');
  }
}

/**
 * Implementation of hook_xc_location().
 *
 * @param $op
 * @param $location
 * @param $params
 */
function xc_sql_xc_location($op, $location, $params = array()) {
  switch ($op) {
    case 'types':
      $types['sql'] = array(
        'name'        => t('Drupal SQL database'),
        'description' => t('Store and retrieve metadata elements to and from the Drupal SQL database'),
        'module'      => 'xc_sql',
        'help'        => t('Store and retrieve metadata elements to and from the Drupal SQL database'),
        'default'     => TRUE
      );
      return $types;
      break;

    case 'view':
    case 'load':
      break;

    case 'form':
      if (is_array($location->types) && in_array('sql', $location->types)) {
        $form['sql']['xc_sql_notice'] = array(
          '#type' => 'item',
          '#title' => t('Notice'),
          '#value' => t('No additional information is necessary to store metadata into the Drupal SQL database')
        );
        return $form;
      }
      break;

    case 'create':
//      if (is_array($location->types) && in_array('sql', $location->types)) {
//        // create a new SQL storage and assign it to the location
//        $sql = "INSERT INTO {xc_sql_storage} (location_id) VALUES ('%d')";
//        $result = db_query($sql, $location->location_id);
//        if ($result) {
//          // sets to the ID instead instead of NULL
//          $storage = db_last_insert_id('{xc_sql_storage}', 'storage_id');
//          drupal_set_message(t('SQL storage created'));
//        }
//        else {
//          drupal_set_message(t('Unexpected error. Unable to create SQL storage'),
//            'error');
//        }
//      }
//      return $return;
        break;

    case 'update':
      break;

    case 'purge':
      $storage = new stdClass();
      $storage->storage_id = xc_sql_get_storage_id($location->location_id);
      xc_sql_storage_empty($storage);
      break;

    case 'delete':
      $storage = new stdClass();
      $storage->storage_id = xc_sql_get_storage_id($location->location_id);
      xc_sql_storage_empty($storage);
      xc_sql_storage_unset($storage);
      break;
  }
}

function xc_sql_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'xc_location_form') {
    $step     = &$form_state['storage']['step'];
    $location = &$form_state['storage']['location'];

    if ($step == 2
        && is_array($location->types)
        && in_array('sql', $location->types)) {
      $form['sql']['sql_notice'] = array(
        '#type' => 'item',
        '#title' => t('Notice'),
        '#value' => t('No additional information is necessary to store metadata into the Drupal SQL database')
      );

      $form['#validate'][] = 'xc_sql_location_validate';
      $form['#submit'][]   = 'xc_sql_location_submit';
    }
  }
}

function xc_sql_location_validate($form, &$form_state) {
  $step     = &$form_state['storage']['step'];
  $location = &$form_state['storage']['location'];

  if ($step == 2
      && is_array($location->types)
      && in_array('sql', $location->types)) {
    $form_state['sql_storage'] = &$form_state['storage'];
  }
}

function xc_sql_location_submit($form, &$form_state) {
  $step     = &$form_state['sql_storage']['step'];
  $location = &$form_state['sql_storage']['location'];

  if ($step > 2
      && is_array($location->types)
      && in_array('sql', $location->types)) {
    $sql = 'SELECT storage_id FROM {xc_sql_storage} WHERE location_id = %d';

    $storage->storage_id  = db_result(db_query($sql, $location->location_id));
    $storage->location_id = $location->location_id;
    xc_sql_storage_set($storage);
    unset($form_state['sql_storage']);
  }
}

/**
 * Implementation of hook_xc_store()
 *
 * Store metadata by INSERT or UPDATE to the Drupal database.
 * Has no return value.
 * @see _xc_store
 *
 * @param $entity Array of XCEntity objects
 * @param $locations Data storage location
 * @param $params Additional arguments
 * @return unknown_type
 */
function xc_sql_xc_store(&$entity, $locations, $params = array()) {
  global $_oaiharvester_statistics, $_xc_sql_metadata_csv, $_oaiharvester_need_debug;
  $start = microtime(TRUE);
  $update = $params['need_update'] ? TRUE : FALSE;
  $use_insert = isset($params['use_insert']) ? $params['use_insert'] : 0;
  $use_hook_call = isset($params['use_hook_call']) ? $params['use_hook_call'] : 0;

  // $definition    = $params['definition'];
  // $qualified     = $params['qualified'];
  // $relationships = $params['relationships'];
  // $properties    = $params['properties'];

  if (!is_array($locations)) {
    $locations = array($locations);
  }

  foreach ($locations as $location) {
    if ($location->has_sql_type) {
      if ($location->location_id && $entity->metadata_id) {
        // Delete it if exists
        $time = time();
        if ($update) {
          $sql = "UPDATE {xc_sql_metadata} SET metadata = '%s', timestamp = %d
                  WHERE metadata_id = %d AND location_id = %d";
          $result = db_query($sql, serialize($entity->metadata), $time, $entity->metadata_id, $location->location_id);
          $entity->stored = TRUE;
        }
        else {
          if ($use_insert == XC_SQL_CMD_INSERT) {
            $sql = 'DELETE FROM {xc_sql_metadata} WHERE metadata_id = %d AND location_id = %d';
            db_query($sql, $entity->metadata_id, $location->location_id);
            $record = (object) array(
              'metadata_id' => $entity->metadata_id,
              'location_id'  => $location->location_id,
              'metadata'    => serialize($entity->metadata),
              'timestamp'   => $time
            );
            drupal_write_record('xc_sql_metadata', $record);
            $entity->stored = TRUE;
          }
          else {
            $metadata = @serialize($entity->metadata);
            if ($metadata === FALSE) {
              xc_log_error('xc sql', 'It was not possible to serialize the metadata: ' . var_export($entity, TRUE));
              $entity->stored = FALSE;
            }
            else {
              $row = $entity->metadata_id . XC_CSV_TAB
                   . $location->location_id . XC_CSV_TAB
                   . $metadata . XC_CSV_TAB
                   . $time . XC_CSV_CR;
              $type = get_resource_type($_xc_sql_metadata_csv);
              if ($type != 'stream') {
                xc_log_error('xc sql', '_xc_sql_metadata_csv is not a stream resource, but ' . $type);
                $entity->stored = FALSE;
              }
              else {
                if (fwrite($_xc_sql_metadata_csv, $row) === FALSE) {
                  xc_log_error('xc sql', 'The writing ' . strlen($row). ' bytes into CSV file was not possible');
                  $entity->stored = FALSE;
                }
                else {
                  $entity->stored = TRUE;
                }
              }
            }
          } // use CSV
        } // new record
      } // has metadata_id
      else {
        xc_log_error('xc sql', 'No location_id or metadata_id: ' . var_export($location, TRUE) . ', ' . var_export($entity, TRUE));
      }
    } // has sql type
    else {
      xc_log_error('xc sql', 'No sql type in location: ' . var_export($location, TRUE));
    }
  } // foreach

  if ($use_hook_call) {
    xc_oaiharvester_statistics_set('01 step1/02 bridge/03 _xc_store/06 hook_xc_store/sql_xc_store', abs(microtime(TRUE) - $start));
  }
  else {
    xc_oaiharvester_statistics_set('01 step1/02 bridge/03 sql_xc_store', abs(microtime(TRUE) - $start));
  }
}

/**
 * Implementation of hook_xc_remove().
 * It remover records from xc_sql_metadata table.
 */
function xc_sql_xc_remove(&$entity, $locations, $params = array()) {
  /*
  $definition    = $params['definition'];
  $qualified     = $params['qualified'];
  $relationships = $params['relationships'];
  $properties    = $params['properties'];
  */

  foreach ($locations as $location) {
    if ($location->has_sql_type) {
      $sql = 'DELETE FROM {xc_sql_metadata} WHERE metadata_id = %d AND location_id = %d';
      $result = db_query($sql, $entity->metadata_id, $location->location_id);
      if (!$result) {
        drupal_set_message(t('Unexpected error. Unable to remove metadata from SQL storage'), 'error');
      }
    }
  }
}

/**
 * Implementation of hook_xc_retrieve().
 *
 * Implements hook_xc_retrieve() to retrieve metadata by SELECT from
 * the Drupal database. Returns an array of XCEntity objects.
 *
 * @see _xc_retrieve
 *
 * @param $entity
 *   Array of arrays of entity type and id pairs
 * @param $locations
 *   Data retrieval location
 * @param $params
 *   Additional arguments
 *
 * @return array Array of XCEntity objects
 */
function xc_sql_xc_retrieve(&$entity, $location = NULL, $params = array()) {
  /*
  if (!empty($param)) {
    $definition    = $params['definition'];
    $qualified     = $params['qualified'];
    $properties    = $params['properties'];
    $relationships = $params['relationships'];
  }
  */

  if (isset($location) && $location->has_sql_type) {
    $sql = 'SELECT metadata FROM {xc_sql_metadata} WHERE metadata_id = %d AND location_id = %d';
    $_object = db_fetch_object(db_query($sql, $entity->metadata_id, $location->location_id));
    if ($entity !== FALSE) {
      $entity->metadata  = unserialize($_object->metadata);
    }
  }
  else {
    $sql = 'SELECT metadata FROM {xc_sql_metadata} WHERE metadata_id = %d';
    $_object = db_fetch_object(db_query($sql, $entity->metadata_id));
    if ($entity !== FALSE) {
      $entity->metadata  = unserialize($_object->metadata);
    }
  }
}

/**
 * Implementation of hook_xc_optimize().
 *
 * Optimizes SQL tables.
 *
 * @param $locations (array)
 *   List of Location objects
 * @param $params (array)
 *   Associative array of parameters. Possible keys are:
 *   - 'solr servers': the array of Solr servers
 *   - 'sql_optimize': the SQL command to optimize table(s)
 */
function xc_sql_xc_optimize($locations = array(), $params = array()) {
  global $db_type;
  static $optimized;

  if (!$optimized) {
    $do = TRUE;
    if (!empty($locations)) {
      $do = FALSE;
      foreach ($locations as $location) {
        if ($location->has_sql_type) {
          $do = TRUE;
          break;
        }
      }
    }

    if ($do) {
      switch ($db_type) {
        case 'mysql':
        case 'mysqli':
          $sql = 'OPTIMIZE TABLE {xc_sql_metadata}';
          break;

        case 'pgsql':
          $sql = 'VACUUM ANALYZE {xc_sql_metadata}';
          break;

        default:
          $sql = check_plain($params['sql_optimize']);
          break;
      }
      xc_log_info('xc sql', 'Optimizing xc_sql_metadata');
      $t = microtime(TRUE);
      $optimized = db_query($sql);
      $duration = microtime(TRUE) - $t;

      xc_log_info('xc sql', 'xc_sql_metadata table is optimized in ', oaiharvester_sec_to_time($tduration));
    }
  }
}
