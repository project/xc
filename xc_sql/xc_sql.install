<?php
/**
 * @file
 * Installations functions for XC SQL module
 *
 * @copyright (c) 2010-2011 eXtensible Catalog Organization
 */

/**
 * Implemetation of hook_schema()
 * @return unknown_type
 */
function xc_sql_schema() {
  $schema['xc_sql_metadata'] = array(
    'description' => t('Table that stores the metadata'),
    'fields' => array(
      'metadata_id' => array(
        'description' => t('Entity instance identifier'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE
      ),
      'location_id' => array(
        'description' => t('Location identifier'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE
      ),
      'metadata' => array(
        'description' => t('Metadata container'),
        'type' => 'text',
        'size' => 'big',
        'not null' => TRUE
      ),
      'timestamp' => array(
        'description' => t('Entity instance last modified time'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE
      )
    ),
    'indexes' => array(
      'location_id' => array('location_id'),
      'metadata_id_location_id' => array('metadata_id', 'location_id'),
      'location_id_metadata_id' => array('location_id', 'metadata_id')
    ),
    'primary key' => array('metadata_id'),
  );

  $schema['xc_sql_storage'] = array(
    'description' => t('Link table for metadata storage locations and SQL database storage'),
    'fields' => array(
      'storage_id' => array(
        'description' => t('Related storage identifier'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE
      ),
      'location_id' => array(
        'description' => t('Related metadata storage location identifier'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE
      )
    ),
    'unique key' => array(
      'location' => array('location'),
      'storage_location' => array('storage_id', 'location_id'),
      'location_storage' => array('location_id', 'storage_id')
    ),
    'primary key' => array('storage_id')
  );

  return $schema;
}

function xc_sql_update_6100() {
  $ret = array();
  $schema = xc_sql_schema();
  $table = 'xc_sql_metadata';
  // drop table xc_sql_metadata
  db_drop_table($ret, 'xc_sql_metadata');
  // create table
  db_create_table($ret, $table, $schema[$table]);
  // drop table xc_sql_storage
  db_drop_table($ret, 'xc_sql_storage');
  return $ret;
}

/**
 * Implementation of hook_install()
 */
function xc_sql_install() {
  drupal_install_schema('xc_sql');
  variable_set('xc_sql_defaults_installed', XC_INSTALLED);
}

/**
 * Implementation of hook_enable()
 *
 * It imports default values
 */
function xc_sql_enable() {
  // imports default sql storage
  if (module_exists('xc_util')) {
    $path = drupal_get_path('module', 'xc_sql') . '/import/';
    if (variable_get('xc_sql_defaults_installed', XC_UNINSTALLED) == XC_INSTALLED) {
      $filename = $path . 'xc_sql_storage.csv';
      xc_util_bulk_insert('xc_sql_storage', xc_util_csv2objects($filename));
      variable_set('xc_sql_defaults_installed', XC_LOADED);
    }
  }
}

/**
 * Implementation of hook_uninstall().
 */
function xc_sql_uninstall() {
  // Destroy all metadata in Drupal
  // This needs some editing...
//  foreach (variable_get('xc_metadata_entities', array()) as $type) {
//    $table = xc_sql_entity_get_table($type);
//    $results = array();
//    if (db_table_exists($table)) {
//      db_drop_table($results, $table);
//    }
//  }

  // Remove tables.
  drupal_uninstall_schema('xc_sql');
  variable_del('xc_sql_defaults_installed');
}
