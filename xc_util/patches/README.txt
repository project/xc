This directory contains some patches, which helps Drupal Toolkit to work with
other modules and the Drupal core.

- hook_fieldset_helper_path_alter-823318-4.patch
  Patch for Fieldset Helper module 1.0 version. In next version of Fieldset Helper
  a similar solution will be built in like the one we suggested, so it won't be
  necessary.

- xc-autocomplete.patch
  Patch for Drupal autocomplete. This is a temporary solution, in next versions of
  Drupal Toolkit we will provide our own solution inside xc module. It works with
  Drupal 6.22.
