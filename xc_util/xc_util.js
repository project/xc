/**
 * @file
 * A utility module, which contains functions usable in other XC modules
 *
 * @copyright (c) 2010-2011 eXtensible Catalog Organization
 */

Drupal.theme.xc_tr = function (str, className) {
  return Drupal.theme('xc_element', 'tr', str, className);
};

Drupal.theme.xc_td = function (str, className) {
  return Drupal.theme('xc_element', 'td', str, className);
};

Drupal.theme.xc_element = function (el, str, className) {
  var temp = $('<' + el + '></' + el + '>');
  if (className != null) {
    temp.addClass(className);
  }
  return temp.html(str).parent().html();
};
