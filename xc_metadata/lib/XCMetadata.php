<?php
/**
 * @file
 * XCMetadata class definition
 *
 * @copyright (c) 2010-2011 eXtensible Catalog Organization
 */

/**
 * Abstract class and parent of all metadata definitions objects
 */
abstract class XCMetadata {
  public $set;
}

