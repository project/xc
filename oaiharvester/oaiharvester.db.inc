<?php
/**
 * @file
 * Database functions for OAI Harvester module
 *
 * @copyright (c) 2010-2011 eXtensible Catalog Organization
 */

/**
 * Return the list of links to the providers which support a given format.
 *
 * @param $format_id (int)
 *   The id of format
 *
 * @return (array)
 *   List of links
 */
function _oaiharvester_providers_get_by_format($format_id) {
  $sql = 'SELECT prov.provider_id, prov.name
            FROM {oaiharvester_formats_to_providers} AS ftp
            LEFT JOIN {oaiharvester_providers} AS prov ON prov.provider_id = ftp.provider_id
              WHERE ftp.format_id = %d
                ORDER BY prov.name';
  $result = db_query($sql, $format_id);
  $providers = array();
  while ($data = db_fetch_object($result)) {
    $providers[] = l(check_plain($data->name),
      'admin/xc/harvester/repository/' . check_plain($data->provider_id));
  }
  return $providers;
}

/**
 * Return the list of links to the providers which support a given set.
 *
 * @param $set_id (int)
 *   The id of set
 *
 * @return (array)
 *   List of links
 */
function _oaiharvester_providers_get_by_set($set_id) {
  $sql = 'SELECT prov.provider_id, prov.name
            FROM {oaiharvester_sets_to_providers} AS stp
            LEFT JOIN {oaiharvester_providers} AS prov ON prov.provider_id = stp.provider_id
              WHERE stp.set_id = %d
                ORDER BY prov.name';
  $result = db_query($sql, $set_id);
  $providers = array();
  while ($data = db_fetch_object($result)) {
    $providers[] = l(check_plain($data->name),
      'admin/xc/harvester/repository/' . check_plain($data->provider_id));
  }
  return $providers;
}

/**
 * Gets repository by an URL. There should be only one repository for an URL.
 *
 * @param $url (string)
 *   The URL of the repository
 *
 * @return (object)
 *   The oaiharvester_providers record belongs to the URL
 */
function _oaiharvester_provider_get_by_url($url) {
  $sql = 'SELECT * from {oaiharvester_providers} WHERE oai_provider_url = \'%s\'';
  $result = db_query($sql, $url);
  $data = db_fetch_object($result);
  return $data;
} // _oaiharvester_get_repository

/**
 * Gets all provider's name.
 *
 * @return (array)
 *   Associative array of provider id and its name
 */
function oaiharvester_provider_get_all_names() {
  $providers = array();
  $sql = 'SELECT provider_id, name from {oaiharvester_providers}';
  $result = db_query($sql);
  while ($data = db_fetch_object($result)) {
    $providers[$data->provider_id] = $data->name;
  }
  return $providers;
}

/**
 * Gets all providers.
 *
 * @return (array)
 *   List of all OAI harvester provider objects
 */
function oaiharvester_provider_get_all() {
  $providers = array();
  $sql = 'SELECT * from {oaiharvester_providers} ORDER BY name';
  $result = db_query($sql);
  while ($data = db_fetch_object($result)) {
    $providers[] = $data;
  }
  return $providers;
}

/**
 * Gets the URL of a data provider by its identifier.
 *
 * @param $provider_id (int)
 *   The identifier of the data provider
 *
 * @return (string)
 *   The URL of the data provider
 */
function _oaiharvester_provider_get_url_by_id($provider_id) {
  static $provider_cache;

  if (!isset($provider_cache[$provider_id])) {
    $sql = 'SELECT oai_provider_url from {oaiharvester_providers} WHERE provider_id = %d';
    $result = db_query($sql, $provider_id);
    $data = db_fetch_object($result);
    $provider_cache[$provider_id] = $data->oai_provider_url;
  }

  return $provider_cache[$provider_id];
} // _oaiharvester_provider_get_url_by_id

/**
 * Deletes all steps of a schedule by its identifier.
 *
 * @param $harvest_schedule_id (int)
 *  The schedule's identifier
 */
function _oaiharvester_schedule_steps_delete($schedule_id) {
  $sql = 'DELETE FROM {oaiharvester_harvest_schedule_steps}
          WHERE schedule_id = %d';
  $result = db_query($sql, $schedule_id);
}

/**
 * Deletes a step.
 *
 * @param $step_id (int)
 *   The step's identifier
 */
function _oaiharvester_schedule_steps_delete_by_stepid($step_id) {
  $sql = 'DELETE FROM {oaiharvester_harvest_schedule_steps}
          WHERE step_id = %d';
  $result = db_query($sql, $step_id);
}

/**
 * Deletes a step.
 *
 * @param $step (object)
 *   A oaiharvester_harvest_schedule_steps object
 */
function _oaiharvester_schedule_step_delete_object($step) {
  _oaiharvester_schedule_step_delete($step->schedule_id, $step->format_id, $step->set_id);
}

function _oaiharvester_schedule_step_delete($schedule_id, $format_id, $set_id) {
  if (empty($set_id)) {
    $sql = 'DELETE FROM {oaiharvester_harvest_schedule_steps}
               WHERE schedule_id = %d
                 AND format_id = %d
                 AND set_id IS NULL';
    $result = db_query($sql, $schedule_id, $format_id);
  }
  else {
    $sql = 'DELETE FROM {oaiharvester_harvest_schedule_steps}
               WHERE schedule_id = %d
                 AND format_id = %d
                 AND set_id = %d';
    $result = db_query($sql, $schedule_id, $format_id, $set_id);
  }
}

/**
 * Sets schedule status to active (running)
 *
 * @param $schedule_id (int)
 *   The ID of the schedule
 *
 * @return (Boolean)
 *   Flag of action's success (true/false)
 */
function oaiharvester_schedule_set_active($schedule_id) {
  return _oaiharvester_schedule_set_status($schedule_id, OAIHARVESTER_STATUS_ACTIVE);
}

/**
 * Sets schedule status to passive (not running)
 *
 * @param $schedule_id (int)
 *   The ID of the schedule
 *
 * @return (Boolean)
 *   Flag of action's success (true/false)
 */
function oaiharvester_schedule_set_passive($schedule_id) {
  return _oaiharvester_schedule_set_status($schedule_id, OAIHARVESTER_STATUS_PASSIVE);
}

/**
 * Sets the status of the schedule (active or passive)
 *
 * @param $schedule_id (int)
 *   The ID of the schedule
 * @param $status (String)
 *   The status of the schedule
 *
 * @return (Boolean)
 *   Flag of action's success (true/false)
 */
function _oaiharvester_schedule_set_status($schedule_id, $status) {
  static $possible_values = array(OAIHARVESTER_STATUS_ACTIVE => 1, OAIHARVESTER_STATUS_PASSIVE => 1);

  if (!isset($possible_values[$status])) {
    return FALSE;
  }

  $schedule = _oaiharvester_schedule_get($schedule_id);
  $schedule->status = $status;
  $ret_val = drupal_write_record('oaiharvester_harvester_schedules', $schedule, 'harvest_schedule_id');
  if ($ret_val == SAVED_UPDATED) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Gets all steps of a schedule.
 *
 * @param $schedule_id (int)
 *   The schedule's identifier
 *
 * @return (array)
 *   The list of step objects. The properties are:
 *   - step_id (int): the step's identifier
 *   - last_ran (date): the last ran of the step
 *   - format_id (int): the format identifier
 *   - name (string): name of format
 *   - namespace (string): namespace of format
 *   - schema_location (string): the schema location (URL) of the format
 *   - set_id (int): set identifier
 *   - display_name (string): The human readable name of the set.
 *   - description (string): The short description about the set's content.
 *   - set_spec (string): The OAI standard's set specification.
 *   - is_provider_set (int): Flag to indicate whether it is a provider set.
 *   - is_record_set (int): Flag to indicate whether it is a record set.
 */
function _oaiharvester_schedule_steps_get($schedule_id) {
  $sql = 'SELECT step.step_id, step.last_ran, fmt.*, sets.*
            FROM {oaiharvester_harvest_schedule_steps} AS step
              INNER JOIN {oaiharvester_formats} AS fmt
                    ON fmt.format_id = step.format_id
               LEFT JOIN {oaiharvester_sets} AS sets
                    ON sets.set_id = step.set_id
                 WHERE schedule_id = %d';
  $result = db_query($sql, $schedule_id);
  $steps = array();
  while ($data = db_fetch_object($result)) {
    $steps[] = $data;
  }
  return $steps;
}

/**
 * Gets the minimal properties of each steps of a schedule.
 *
 * @param $schedule_id (int)
 *   The schedule identifier
 *
 * @return (array)
 *   The list of step objects. The properties are:
 *   - step_id (int): the step's identifier
 *   - last_ran (date): the last ran of the step
 *   - schedule_id (int): the schedule identifier
 *   - format_id (int): the format identifier
 *   - set_id (int): set identifier
 */
function _oaiharvester_schedule_steps_get_atomic($schedule_id) {
  $sql = 'SELECT step_id, schedule_id, format_id, set_id, last_ran
            FROM {oaiharvester_harvest_schedule_steps}
              WHERE schedule_id = %d';
  $result = db_query($sql, $schedule_id);
  $steps = array();
  while ($data = db_fetch_object($result)) {
    $steps[] = $data;
  }
  return $steps;
}

/**
 * Reset the last ran value to null for a given schedule step
 *
 * @param $step_id (int)
 *   The step identifier
 *
 * @return (boolean)
 */
function _oaiharvester_schedule_step_reset($step_id) {
  $sql = 'UPDATE {oaiharvester_harvest_schedule_steps}
             SET last_ran = NULL
             WHERE step_id = %d';
  $result = db_query($sql, $step_id);
  return $result;
}

/**
 * Gets the atomic steps of a schedule. Each steps has the following fields:
 * step_id, schedule_id, format_id, set_id, last_ran
 *
 * @param $schedule_id (int)
 *   The schedule identifier
 * @param $format_name (string)
 *   The format name (the OAI-PMH format parameter)
 * @param $set_spec (string)
 *   The set specification (OAI-PMH SetSpec parameter)
 *
 * @return (array)
 *   The list of set objects.
 */
function _oaiharvester_schedule_step_get_atomic($schedule_id, $format_name, $set_spec = NULL, $schedule_type = OAIHARVESTER_PROVIDERTYPE_SERVER) {
  $sql = 'SELECT step.*
            FROM {oaiharvester_harvest_schedule_steps} step
            LEFT JOIN {oaiharvester_formats} AS format
                    ON format.format_id = step.format_id';
  if ($set_spec != NULL) {
    $sql .= ' LEFT JOIN {oaiharvester_sets} AS sets
                    ON sets.set_id = step.set_id';
  }
  $sql .= ' WHERE schedule_id = %d';

  if (!empty($format_name)) {
    $sql .= " AND format.name = '%s'";
  }

  if ($set_spec != NULL) {
    $sql .= " AND sets.set_spec = '%s'";
  }
  else {
    $sql .= ' AND step.set_id IS NULL';
  }

  $result = db_query($sql, $schedule_id, $format_name, $set_spec);
  $steps = array();
  while ($data = db_fetch_object($result)) {
    $steps[] = $data;
  }
  return $steps;
}

/**
 * Gets schedule by identifier.
 *
 * @param $id (int)
 *   The schedule identifier
 *
 * @return (onject)
 *   The schedule object
 */
function _oaiharvester_schedule_get($id) {
  static $cache;
  if (!isset($cache[$id])) {
    $sql = 'SELECT * from {oaiharvester_harvester_schedules} WHERE harvest_schedule_id = %d';
    $result = db_query($sql, $id);
    $data = db_fetch_object($result);
    $cache[$id] = $data;
  }
  return $cache[$id];
} // _oaiharvester_schedule_get

/**
 * Gets schedule by provider id.
 *
 * @param $id (int)
 *   The ID of data provider
 *
 * @return (Array)
 *   List of links to schedules
 */
function _oaiharvester_schedule_get_by_provider($provider_id) {
  static $cache;
  if (!isset($cache[$provider_id])) {
    $sql = 'SELECT * FROM {oaiharvester_harvester_schedules} WHERE provider_id = %d';
    $result = db_query($sql, $provider_id);
    $links = array();
    while ($data = db_fetch_object($result)) {
      $links[] = l($data->schedule_name, 'admin/xc/harvester/schedule/' . $data->harvest_schedule_id);
    }
    $cache[$provider_id] = $links;
  }
  return $cache[$provider_id];
} // _oaiharvester_schedule_get

/**
 * Get all schedule data, and the provider name and URL
 *
 * @param $schedule_id (int)
 *   The schedule ID
 *
 * @return (object)
 *   The schedule's properties including the data provider's properties.
 */
function _oaiharvester_schedule_get_full($schedule_id) {
  static $cache;

  if (!isset($cache[$schedule_id])) {
    $sql = 'SELECT schedule.*,
                   provider.name AS repository_name,
                   provider.oai_provider_url,
                   provider.oai_identifier,
                   provider.type,
                   (DATE(end_date) >= CURDATE()) AS active
            FROM {oaiharvester_harvester_schedules} AS schedule
              LEFT JOIN {oaiharvester_providers} AS provider
                     ON provider.provider_id = schedule.provider_id
            WHERE schedule.harvest_schedule_id = %d';
    $result = db_query($sql, $schedule_id);
    $data = db_fetch_object($result);
    if ($data && !is_null($data)) {
      $data->oai_identifier = unserialize($data->oai_identifier);
    }
    $cache[$schedule_id] = $data;
  }
  return $cache[$schedule_id];
} // _oaiharvester_schedule_get_full

/**
 * Gets all schedule data, and the provider name and URL.
 */
function _oaiharvester_schedule_get_all() {
  $sql = 'SELECT schedule.*,
                 provider.name AS repository_name,
                 provider.oai_provider_url,
                 provider.oai_identifier,
                 provider.type,
                 (DATE(end_date) >= CURDATE()) AS active
          FROM {oaiharvester_harvester_schedules} AS schedule
            LEFT JOIN {oaiharvester_providers} AS provider
                   ON provider.provider_id = schedule.provider_id
          ORDER BY name';
  $result = db_query($sql);
  $schedules = array();
  while ($data = db_fetch_object($result)) {
    $data->oai_identifier = unserialize($data->oai_identifier);
    $schedules[] = $data;
  }

  return $schedules;
} // _oaiharvester_schedule_get_all

/**
 * Deletes a schedule from the database.
 *
 * @param $schedule_id (int)
 *   The identifier of the schedule
 *
 * @return (boolean)
 *   TRUE if the record were successfully deleted, FALSE if the query was not executed correctly.
 */
function _oaiharvester_schedule_delete($schedule_id) {
  $sql = 'DELETE FROM {oaiharvester_harvester_schedules}
          WHERE harvest_schedule_id = %d';
  return db_query($sql, $schedule_id);
} // _oaiharvester_schedule_delete

/**
 * Gets sets of a provider.
 *
 * @param $provider_id (int)
 *   The ID of provider
 *
 * @return (resource)
 *   Database query result resource or FALSE
 */
function _oaiharvester_sets_get_by_provider($provider_id) {
  static $cache;

  if (!isset($cache[$provider_id])) {
    $sql = 'SELECT sets.* '
         .   'FROM {oaiharvester_sets} AS sets, '
         .        '{oaiharvester_sets_to_providers} AS con'
         . ' WHERE sets.set_id = con.set_id'
         .   ' AND con.provider_id = %d';
    $result = db_query($sql, $provider_id);
    $cache[$provider_id] = $result;
  }
  return $cache[$provider_id];
} // _oaiharvester_sets_get_by_provider

/**
 * Gets the metadata formats by a provider.
 *
 * @param $provider_id (int)
 *   The ID of a provider
 *
 * @return (resource)
 *   Database query result resource or FALSE
 */
function _oaiharvester_metadataformats_get_by_provider($provider_id) {
  $sql = 'SELECT formats.*
           FROM {oaiharvester_formats} AS formats,
                {oaiharvester_formats_to_providers} AS con'
       . ' WHERE formats.format_id = con.format_id'
       .   ' AND con.provider_id = %d';
  return db_query($sql, $provider_id);
}

/**
 * Gets the whole queue.
 *
 * @return (resource)
 *   Database query result resource or FALSE
 */
function _oaiharvester_queue_get() {
  $sql = 'SELECT * FROM {oaiharvester_harvest_queue}';
  return db_query($sql);
}

function _oaiharvester_queue_count() {
  return db_result(db_query('SELECT count(*) FROM {oaiharvester_harvest_queue}'));
}

/**
 * Gets a queue item by its ID.
 *
 * @param $queue_id (int)
 *   The queue identifier
 *
 * @return (object)
 *   A oaiharvester_harvest_queue table record
 */
function _oaiharvester_queue_get_item_by_id($queue_id) {
  $sql = 'SELECT * FROM {oaiharvester_harvest_queue} WHERE harvest_id = %d';
  return db_fetch_object(db_query($sql, $queue_id));
}

/**
 * Deletes a queue by identifier.
 *
 * @param $queue_id (int)
 *   The queue identifier
 */
function _oaiharvester_queue_delete_item_by_id($queue_id) {
  $sql = 'DELETE FROM {oaiharvester_harvest_queue} WHERE harvest_id = %d';
  return db_query($sql, $queue_id);
} // _oaiharvester_queue_delete_item_by_id

/**
 * Deletes all queues.
 */
function _oaiharvester_queue_delete_all() {
  return db_query('DELETE FROM {oaiharvester_harvest_queue}');
} // _oaiharvester_queue_delete_all

/**
 * Returns a queue item connected to a given schedule.
 *
 * @param $schedule_id (int)
 *   The schedule ID
 * @param $format (string)
 *   The metadata format's name to harvest
 * @param $set (string)
 *   The set spec to harvest
 *
 * @return (resource)
 *   A database query result resource, or FALSE if the query was not executed
 *   correctly.
 */
function _oaiharvester_queue_get_item_by_schedule($schedule_id, $format_name,
    $set_spec) {
  $sql = 'SELECT * FROM {oaiharvester_harvest_queue}
            WHERE harvest_schedule_id = %d
              AND metadata_prefix = \'%s\'';
  if (!empty($set)) {
    $sql .= ' AND set_name = \'%s\'';
    return db_query($sql, $schedule_id, $format_name, $set_spec);
  }
  $sql .= ' AND set_name IS NULL';
  return db_query($sql, $schedule_id, $format_name);
} // _oaiharvester_queue_get_item_by_schedule
