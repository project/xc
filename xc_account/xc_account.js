/**
 * @file
 * JavaScript functions for XC Account module
 *
 * @copyright (c) 2010-2011 eXtensible Catalog Organization
 */
var XCSearch = {};
if (Drupal.jsEnabled) {

  $(document).ready(function(){
    $('.select-all-action').click(function(){
      $('#xc-account-bookmarked-items-form input[type=checkbox]').each(function(){
        if (!this.checked) {
          this.checked = 'checked';
        }
      });
    });

    $('.select-none-action').click(function() {
      $('#xc-account-bookmarked-items-form input[type=checkbox]').each(function() {
        if (this.checked) {
          this.checked = false;
        }
      });
    });

    $('#email_action').click(function() {
      document.location = Drupal.settings.mail_url + XCSearch.getSelectedItems().join(',')
        + '?' + Drupal.settings.destination;
    });

    $('#print_action').click(function() {
      document.location = Drupal.settings.print_url + XCSearch.getSelectedItems().join(',');
    });

    $('.ui-dialog-buttonpane button:first').attr('id', 'cancel-button').html(Drupal.t('Cancel'));
    $('.ui-dialog-buttonpane button:last').attr('id', 'yes-button').html(Drupal.t('Yes'));

    $('#remove-bookmark').click(function() {
      var selectedItems = XCSearch.getSelectedItems();
      var count = selectedItems.length;
      if (count > 0) {
        $('#xc-remove-bookmark-number').html(Drupal.formatPlural(count, 'this 1 item', 'these @count items'));
        $('#xc-account-bookmarked-items-form').submit();
      }
      return false;
    });

  });

  XCSearch.changeButtons = function() {
    alert('changeButtons');
    $('.ui-dialog-buttonpane button:first').html('Delete all items');
    $('.ui-dialog-buttonpane button:last').html(Drupal.t('Cancel'));
  },

  /**
   * Get the selected items in a search result list
   */
  XCSearch.getSelectedItems = function() {
    var selectedItems = [];
    $('#xc-account-bookmarked-items-form input[type=checkbox]').each(function() {
      if (this.checked) {
        selectedItems.push(this.value);
      }
    });
    return selectedItems;
  }
}