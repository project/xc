<?php
/**
 * @file
 * Installation functions for XC Account module
 *
 * @copyright (c) 2010-2011 eXtensible Catalog Organization
 */

function xc_account_install() {
  drupal_install_schema('xc_account');
}

function xc_account_uninstall() {
  drupal_uninstall_schema('xc_account');
}

/**
 * Implementation of hook_schema()
 */
function xc_account_schema() {
  $schema['xc_account_bookmarked_items'] = array(
    'description' => t('The place to save items.'),
    'fields' => array(
      'id' => array(
        'description' => t('The identifier of the record.'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE
      ),
      'uid' => array(
        'type' => 'int',
        'description' => t('Drupal user primary identifier'),
        'not null' => TRUE
      ),
      'nid' => array(
        'type' => 'int',
        'description' => t('The item\'s node identifier'),
        'not null' => TRUE
      ),
      'sid' => array(
        'type' => 'varchar',
        'description' => t('The item\'s Solr identifier'),
        'length' => 128,
        'not null' => TRUE
      ),
    ),
    'indexes' => array(
      'id' => array('id'),
    ),
    'primary key' => array('id'),
  );

  return $schema;
}

function xc_account_update_6000() {

    $schema['xc_account_saved_items'] = array(
    'description' => t('The place to save items.'),
    'fields' => array(
      'id' => array(
        'description' => t('The identifier of the record.'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE
      ),
      'uid' => array(
        'type' => 'int',
        'description' => t('Drupal user primary identifier'),
        'not null' => TRUE
      ),
      'nid' => array(
        'type' => 'int',
        'description' => t('The item\'s node identifier'),
        'not null' => TRUE
      ),
      'sid' => array(
        'type' => 'varchar',
        'description' => t('The item\'s Solr identifier'),
        'length' => 128,
        'not null' => TRUE
      ),
    ),
    'indexes' => array(
      'id' => array('id'),
    ),
    'primary key' => array('id'),
  );

  $ret = array();
  if (!db_table_exists('xc_account_saved_items')) {
    db_create_table($ret, 'xc_account_saved_items', $schema['xc_account_saved_items']);
  }
  return $ret;
}

/**
 * Rename xc_account_saved_items to xc_account_bookmarked_items
 */
function xc_account_update_6101() {
  $ret = array();
  if (db_table_exists('xc_account_saved_items')
      && !db_table_exists('xc_account_bookmarked_items')) {
    db_rename_table($ret, 'xc_account_saved_items', 'xc_account_bookmarked_items');
  }
  return $ret;
}