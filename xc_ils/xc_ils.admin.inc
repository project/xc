<?php
/**
 * @file
 * The XC ILS is a small mmodule which helps to use ILS-specific settings in the Drupal Toolkit.
 *
 * We currently use only one of the possible settings: mapping the bibliographic
 * record identifier to an NCIP server. When the DT display an XC record, the
 * XC ILS module give the name of the identifier field, and the content of that
 * field will be used by the NCIP protocol.
 *
 * The module use the xc_ils_settings table, which have the following fields:
 * - <code>settings_id</code> The identifier of the record
 * - <code>ncip_provider_id</code> The identifier of the NCIP server record in
 *   database
 * - <code>book_id_field<code> The name of the bibliographical identifier field
 *
 * @copyright (c) 2010-2011 eXtensible Catalog Organization
 */

function xc_ils_delete_form(&$form_state, $record) {
  $form['id'] = array(
    '#type' => 'value',
    '#value' => $record->settings_id
  );
  return confirm_form($form,
    t('Are you sure you want to remove this ILS mapping?'),
    'admin/xc/ils/list', '', t('Remove'), t('Cancel'));
}

function xc_ils_delete_form_submit($form, &$form_state) {
  $id = $form_state['values']['id'];
  $sql = 'DELETE FROM {xc_ils_settings} WHERE settings_id = %d';
  $result = db_query($sql, $id);
  if ($result == SAVED_DELETED) {
    drupal_set_message(t('ISL settings removed'));
    $form_state['redirect'] = 'admin/xc/ils/list';
  }
  else {
    drupal_set_message(t('Unexpected error. Failed to remove ILS settings.'));
  }
}

function xc_ils_disable_form(&$form_state, $record) {
  $form['id'] = array(
    '#type' => 'value',
    '#value' => $record->settings_id
  );

  return confirm_form(
    $form,
    t('Are you sure you want to disable this ILS mapping?'),
    'admin/xc/ils/list', '', t('Disable'), t('Cancel')
  );
}

function xc_ils_disable_form_submit($form, &$form_state) {
  $id = $form_state['values']['id'];
  $sql = 'UPDATE {xc_ils_settings} SET is_enabled = 0 WHERE settings_id = %d';
  $result = db_query($sql, $id);
  if ($result) {
    drupal_set_message(t('ISL settings disabled'));
  }
  else {
    drupal_set_message(t('Unexpected error. Failed to disabled ILS settings.'));
  }
  $form_state['redirect'] = 'admin/xc/ils/list';
}

function xc_ils_enable_form(&$form_state, $record) {
  $form['id'] = array(
    '#type' => 'value',
    '#value' => $record->settings_id
  );

  return confirm_form(
    $form,
    t('Are you sure you want to enable this ILS mapping?'),
    'admin/xc/ils/list', '', t('Enable'), t('Cancel')
  );
}

function xc_ils_enable_form_submit($form, &$form_state) {
  $id = $form_state['values']['id'];
  $sql = 'UPDATE {xc_ils_settings} SET is_enabled = 1 WHERE settings_id = %d';
  $result = db_query($sql, $id);
  if ($result) {
    drupal_set_message(t('ISL settings enabled'));
  }
  else {
    drupal_set_message(t('Unexpected error. Failed to enabled ILS settings.'));
  }
  $form_state['redirect'] = 'admin/xc/ils/list';
}

